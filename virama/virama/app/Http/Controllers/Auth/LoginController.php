<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        
    }

    


    public function getCredentials($request)
    {
        die;
        $credentials = $request->only($this->loginUsername(), 'password');

        return array_add($credentials, 'is_sdm', '1');
    }

    protected function authenticated($request)
    {
        $user = Auth::user();
        if ($user->is_sdm == 1) {
            // Authentication passed...
            return redirect()->intended('/');
        } else {
            // Session::flush();
            Auth::logout();
        }
    }

    // public function authenticate()
    // {
    //     if (Auth::attempt(['email' => $email, 'password' => $password, 'is_sdm'=>4])) {
    //         // Authentication passed...
    //         return redirect()->intended('/');
    //     } else {
    //         Auth::logout();
    //     }
    // }
    
}
