<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\User;
use Validator;
use Response;
//require ‘vendor/autoload.php’;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request as GuzzleRequest;



class backupcontroller extends Controller
{
	protected $client_id 	= 'mYq3t6w9z$C&F)J@NcRfTjWnZr4u7x!A';
	protected $secret_key	= 's6v9y$B&E)H@McQfTjWnZr4t7w!z%C*F-JaNdRgUkXp2s5v8y/A?D(G+KbPeShVmYq3t6w9z$C&E)H@McQfTjWnZr4u7x!A%D*G-JaNdRgUkXp2s5v8y/B?E(H+MbPeShVmYq3t6w9z$C&F)J@NcRfTjWnZr4u7x!A%D*G-KaPdSgVkXp2s5v8y/B?E(H+MbQeThWmZq3t6w9z$C&F)J@NcRfUjXn2r5u7x!A%D*G-KaPdSgVkYp3s6v9y/B?E(H+MbQeThWmZq4t7w!z%C&F)J@NcRfUjXn2r5u8x/A?D(G-KaPdSgVkYp3s6v9y$B&E)H@MbQeThWmZq4t7w!z%C*F-JaNdRfUjXn2r5u8x/A?D(G+KbPeShVkYp3s6v9y$B&E)H@McQfTjWnZq4t7w!z%C*F-JaNdRgUkXp2s5u8x/A?D(G+KbPeShVmYq3t6w9y$B&E)H@McQfTjWnZr4u7x!A%C*F-JaNdRgUkXp2s5v8y/B?E(G+KbPeShVmYq';
	protected $url_api		= 'http://localhost/viramaAPI/';

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(),[
		'email'=>'required|email',
		'name'=>'required',
		'password'=>'required',
		'confirm_password'=>'required|same:password',
		'status'=>'required',
		]);
		
		if($validator->fails()){
			return response()->json(['error'=>$validator->errors()],401);
		}
		
		$input = $request->all();
		$input['password']= bcrypt($input['password']);
		$user = User::create($input);
		$success['token'] = $user->createToken('nApp')->accessToken;
		$success['id']= $user->id;
		$success['status']=$user->status;
		return response()->json($success, 200);
		

	}
	
	public function upload(Request $request){
		$userid = $request->input('userid');
		$answerprofil1 = $request->input('profil1');
		$answerprofil2 = $request->input('profil2');
		$answerprofil3 = $request->input('profil3');
		$answerprofil4 = $request->input('profil4');
		$answerprofil5 = $request->input('profil5');
		$answerprofil6 = $request->input('profil6');
		$answerprofilhp = $request->input('profilhp');
		$answerprofilnpwp = $request->input('profilnpwp');
		$answerprofilkota = $request->input('profilkota');
		if($answerprofil6==null){$pathprofil6="0";}
		else{
			$pathprofil6 = "foto_diri-".time().".png";
			Storage::disk('public')->put($pathprofil6, base64_decode($answerprofil6));}
		$answerprofil7 = $request->input('profil7');
		if($answerprofil7==null){$pathprofil7="0";}
		else{
			$pathprofil7 = "ktp-".time().".png";
			Storage::disk('public')->put($pathprofil7, base64_decode($answerprofil7));}
		$answerprofil8 = $request->input('profil8');
		if($answerprofil8==null){$pathprofil8="0";}
		else{
			$pathprofil8 = "tanda_tangan-".time().".png";
			Storage::disk('public')->put($pathprofil8, base64_decode($answerprofil8));}
		$answerprofil9 = $request->input('profil9');
		if($answerprofil9==null){$pathprofil9="0";}
		else{
			$pathprofil9 = "npwp-".time().".png";
			Storage::disk('public')->put($pathprofil9, base64_decode($answerprofil9));}
		$answerprofil10 = $request->input('profil10');
		if($answerprofil10==null){$pathprofil10="0";}
		else{
			$pathprofil10 = "buku_tabungan-".time().".png";
			Storage::disk('public')->put($pathprofil10, base64_decode($answerprofil10));}
		$answerprofil11 = $request->input('profil11');
		if($answerprofil11==null){$pathprofil11="0";}
		else{
			$pathprofil11 = "spt_terakhir-".time().".png";
			Storage::disk('public')->put($pathprofil11, base64_decode($answerprofil11));}
		
		$answerpendidikan1 = $request->input('pendidikan1');
		$answerpendidikan2 = $request->input('pendidikan2');
		$answerpendidikan3 = $request->input('pendidikan3');
		$answerpendidikan4 = $request->input('pendidikan4');
		$answerpendidikan5 = $request->input('pendidikan5');
		$answerpendidikan6 = $request->input('pendidikan6');
		$answerpendidikan7 = $request->input('pendidikan7');
		$answerpendidikan8 = $request->input('pendidikan8');
		$answerpendidikan9 = $request->input('pendidikan9');
		$answerpendidikan10 = $request->input('pendidikan10');
		if($answerpendidikan10==null){$pathpendidikan10="0";}
		else{
			$pathpendidikan10 = "ijasah_terakhir-".time().".png";
			Storage::disk('public')->put($pathpendidikan10, base64_decode($answerpendidikan10));}
		$answerpendidikan11 = $request->input('pendidikan11');
		if($answerpendidikan11==null){$pathpendidikan11="0";}
		else{
			$pathpendidikan11 = "transkrip_nilai-".time().".png";
			Storage::disk('public')->put($pathpendidikan11, base64_decode($answerpendidikan11));}
		$answerahli1 = $request->input('ahlistring1');
		$answerahli7 = $request->input('ahli7');
		$answerahli8 = $request->input('ahli8');
		$answerahli9 = $request->input('ahli9');
		$answerahli10 = $request->input('ahli10');
		$answerahli11 = $request->input('ahli11');
		if($answerahli11==null){$pathahli11="0";}
		else{
			$pathahli11 = "sertifikat_keahlian-".time().".png";
			Storage::disk('public')->put($pathahli11, base64_decode($answerahli11));}
		$answerahli12 = $request->input('ahli12');
		if($answerahli11==null){$pathahli12="0";}
		else{
			$pathahli12 = "sertifikat_penghargaan-".time().".png";
			Storage::disk('public')->put($pathahli12, base64_decode($answerahli12));}
		$answerahli13 = $request->input('ahli13');
		if($answerahli13==null){$pathahli13="0";}
		else{
		$pathahli13 = "pernyataan_kesediaan-".time().".png";
		Storage::disk('public')->put($pathahli13, base64_decode($answerahli13));}

		$answergaji1 = $request->input('gaji1');

		
		$User = DB::table('answer_register')->insert(['user_id'=>$userid,'answerprofil1'=>$answerprofil1,'answerprofil2'=>$answerprofil2,'answerprofil3'=>$answerprofil3,'answerprofil4'=>$answerprofil4,'answerprofil5'=>$answerprofil5,
		'answerprofil6'=>$pathprofil6,'answerprofil7'=>$pathprofil7,'answerprofil8'=>$pathprofil8,'answerprofil9'=>$pathprofil9,'answerprofil10'=>$pathprofil10,'answerprofil11'=>$pathprofil11,
		'answerpendidikan1'=>$answerpendidikan1,'answerpendidikan2'=>$answerpendidikan2,'answerpendidikan3'=>$answerpendidikan3,'answerpendidikan4'=>$answerpendidikan4,'answerpendidikan5'=>$answerpendidikan5,'answerpendidikan6'=>$answerpendidikan6,
		'answerpendidikan7'=>$answerpendidikan7,'answerpendidikan8'=>$answerpendidikan8,'answerpendidikan9'=>$answerpendidikan9,'answerpendidikan10'=>$pathpendidikan10,'answerpendidikan11'=>$pathpendidikan11,
		'answerahli1'=>$answerahli1,'answerahli7'=>$answerahli7,'answerahli8'=>$answerahli8,'answerahli9'=>$answerahli9,'answerahli10'=>$answerahli10,'answerahli11'=>$pathahli11,'answerahli12'=>$pathahli12,'answerahli13'=>$pathahli13,
		'answergaji1'=>$answergaji1,'nohp'=>$answerprofilhp,'npwp'=>$answerprofilnpwp,'kota'=>$answerprofilkota]);
		
		$success['success']='1';
		return response()->json($success, 200);
	}
	
	public function uploadkerja(Request $request){
		$userid = $request->input('userid');
		$status = $request->input('status');
		$answerkerja1 = $request->input('kerja1');
		$answerkerja2 = $request->input('kerja2');
		$answerkerja3 = $request->input('kerja3');
		$answerkerja4 = $request->input('kerja4');
		$answerkerja5 = $request->input('kerja5');
		$answerkerja6 = $request->input('kerja6');
		$answerkerja7 = $request->input('kerja7');
		$answerkerja8 = $request->input('kerja8');
		$answerkerja9 = $request->input('kerja9');
		$answerkerja10 = $request->input('kerja10');
		$answerkerja11 = $request->input('kerja11');
		if($answerkerja11==null){$pathkerja11="0";}
		else{
			$pathkerja11 = "surat_keterangan-".time().".png";
			Storage::disk('public')->put($pathkerja11, base64_decode($answerkerja11));}

		$User = DB::table('answer_pengalaman')->insert(['user_id'=>$userid,'answerkerja1'=>$answerkerja1,'answerkerja2'=>$answerkerja2,'answerkerja3'=>$answerkerja3,'answerkerja4'=>$answerkerja4,'answerkerja5'=>$answerkerja5,'answerkerja6'=>$answerkerja6
		,'answerkerja7'=>$answerkerja7,'answerkerja8'=>$answerkerja8,'answerkerja9'=>$answerkerja9,'answerkerja10'=>$answerkerja10,'answerkerja11'=>$pathkerja11]);
		$Change = DB::table('users')->where('id',$userid)->update(['status'=>$status]);
		$success['success']=1;
		$success['jawab']=$answerkerja1;
		return response()->json($success, 200);
		
	}
	
	public function updatekerja(Request $request){
		$userid = $request->input('userid');
		$status = $request->input('status');
		$answerkerja1 = $request->input('kerja1');
		$answerkerja2 = $request->input('kerja2');
		$answerkerja3 = $request->input('kerja3');
		$answerkerja4 = $request->input('kerja4');
		$answerkerja5 = $request->input('kerja5');
		$answerkerja6 = $request->input('kerja6');
		$answerkerja7 = $request->input('kerja7');
		$answerkerja8 = $request->input('kerja8');
		$answerkerja9 = $request->input('kerja9');
		$answerkerja10 = $request->input('kerja10');
		$answerkerja11 = $request->input('kerja11');
		if($answerkerja11==null){$pathkerja11="0";}
		else{
			$pathkerja11 = "surat_keterangan-".time().".png";
			Storage::disk('public')->put($pathkerja11, base64_decode($answerkerja11));}

		$User = DB::table('answer_pengalaman')->where('user_id',$userid)->update(['user_id'=>$userid,'answerkerja1'=>$answerkerja1,'answerkerja2'=>$answerkerja2,'answerkerja3'=>$answerkerja3,'answerkerja4'=>$answerkerja4,'answerkerja5'=>$answerkerja5,'answerkerja6'=>$answerkerja6
		,'answerkerja7'=>$answerkerja7,'answerkerja8'=>$answerkerja8,'answerkerja9'=>$answerkerja9,'answerkerja10'=>$answerkerja10,'answerkerja11'=>$pathkerja11]);
		$Change = DB::table('users')->where('id',$userid)->update(['status'=>$status]);

		$success['success']=1;
		$success['jawab']=$answerkerja1;
		return response()->json($success, 200);
		
	}

	
	public function login(Request $request)
	{
		if(Auth::attempt(['email'=>request('email'),'password'=>request('password')])){
			$user=Auth::user();
			$success['token']= $user->createToken('nApp')->accessToken;
			$success['id']=$user->id;
			$success['status']=$user->status;
			return response()->json($success, 200);
		}
		else{
			return response()->json(['error'=>'Unauthorised'],401);
		}
	}
	
	public function details()
	{
		$user=Auth::user();
		return response()->json(['success'=>$user],200);
	}
	
	public function forgot(Request $request)
	{
		$email = $request->input('email');
		$User = DB::table('users')->where('email', $email)->update(['password'=>bcrypt('viramakarya')]);
		$success['password']='viramakarya';
		$success['sender']='akhmadreza.fathan@gmail.com';
		$success['emailpassword']='liverpool';
		return response()->json($success,200);
	}
	
	public function change(Request $request){
		$id = $request->input('id');
	
		$oldpassword = $request->input('oldpassword');
		$newpassword = $request->input('newpassword');
		$newcpassword = $request->input('newcpassword');
		$validator = Validator::make($request->all(),['newpassword'=>'required','newcpassword'=>'required|same:newpassword',]);
		if($validator->fails()){
			$success['pesan']='Validasi gagal password baru';
			return response()->json($success,401);
		}else{
			$newbcrypt = bcrypt($newpassword);

			if(Auth::attempt(['id'=>$id,'password'=>$oldpassword])){
				$user=Auth::user()->update(['password'=>$newbcrypt]);
				$success['success']='1';
				return response()->json($success,200);
			}
			else{
				$success['success']='Validasi Password Baru';
				return response()->json($success,200);
			}
		}	
	}
	public function viewprofile(Request $request){
		$id= $request->input('user_id');
		$User=DB::table('users')->where('id',$id);
		$success['foto']=$User->first()->foto;
		$success['name']=$User->first()->name;
		$success['nohp']=$User->first()->nohp;
		$success['email']=$User->first()->email;
		return response()->json($success, 200);
		
	}
	public function edit(Request $request){
		$id = $request->input('user_id');
		$foto = $request->input('foto');
				
		if($foto==null){$pathfoto="0";}
		else{
			$pathfoto = "foto_profil-".time().".png";
			Storage::disk('public')->put($pathfoto, base64_decode($foto));}
		$name = $request->input('name');
		$nohp = $request->input('nohp');
		$email = $request->input('email');

		$User = DB::table('users')->where('id',$id)->update(['foto'=>$pathfoto,'name'=>$name,'nohp'=>$nohp,'email'=>$email]);
		$success['success']=1;
		return response()->json($success,200);
	}
	
	public function rejectprofil(Request $request){
		$user_id = $request->input('user_id');
		$User = DB::table('answer_register')->where('user_id',$user_id);
		$success['answerprofil1']=$User->first()->answerprofil1;
		$success['answerprofil2']=$User->first()->answerprofil2;
		$success['answerprofil3']=$User->first()->answerprofil3;
		$success['answerprofil4']=$User->first()->answerprofil4;
		$success['answerprofil5']=$User->first()->answerprofil5;
		$success['answerprofil6']=$User->first()->answerprofil6;
		$success['answerprofil7']=$User->first()->answerprofil7;
		$success['answerprofil8']=$User->first()->answerprofil8;
		$success['answerprofil9']=$User->first()->answerprofil9;
		$success['answerprofil10']=$User->first()->answerprofil10;
		$success['answerprofil11']=$User->first()->answerprofil11;
		$success['answerprofilhp']=$User->first()->nohp;
		$success['answerprofilnpwp']=$User->first()->npwp;
		$success['answerprofilkota']=$User->first()->kota;
		return response()->json($success,200);
	}
	
	public function rejectpendidikan(Request $request){
		$user_id = $request->input('user_id');
		$User = DB::table('answer_register')->where('user_id',$user_id);
		$success['answerpendidikan1']=$User->first()->answerpendidikan1;
		$success['answerpendidikan2']=$User->first()->answerpendidikan2;
		$success['answerpendidikan3']=$User->first()->answerpendidikan3;
		$success['answerpendidikan4']=$User->first()->answerpendidikan4;
		$success['answerpendidikan5']=$User->first()->answerpendidikan5;
		$success['answerpendidikan6']=$User->first()->answerpendidikan6;
		$success['answerpendidikan7']=$User->first()->answerpendidikan7;
		$success['answerpendidikan8']=$User->first()->answerpendidikan8;
		$success['answerpendidikan9']=$User->first()->answerpendidikan9;
		$success['answerpendidikan10']=$User->first()->answerpendidikan10;
		$success['answerpendidikan11']=$User->first()->answerpendidikan11;
		return response()->json($success, 200);
	}
	
	public function rejectahli(Request $request){
		$user_id = $request->input('user_id');
		$User = DB::table('answer_register')->where('user_id',$user_id);
		$success['answerahli1']=$User->first()->answerahli1;
		$success['answerahli7']=$User->first()->answerahli7;
		$success['answerahli8']=$User->first()->answerahli8;
		$success['answerahli9']=$User->first()->answerahli9;
		$success['answerahli10']=$User->first()->answerahli10;
		$success['answerahli11']=$User->first()->answerahli11;
		$success['answerahli12']=$User->first()->answerahli12;
		$success['answerahli13']=$User->first()->answerahli13;
		return response()->json($success, 200);
	}
	
	public function rejectgaji(Request $request){
		$user_id = $request->input('user_id');
		$User = DB::table('answer_register')->where('user_id',$user_id);
		$success['answergaji1']=$User->first()->answergaji1;
		return response()->json($success,200);
	}
	public function rejectkerja(Request $request){
		$user_id = $request->input('user_id');
		$User = DB::table('answer_pengalaman')->where('user_id',$user_id);
		$success['answerkerja1']=$User->first()->answerkerja1;
		$success['answerkerja2']=$User->first()->answerkerja2;
		$success['answerkerja3']=$User->first()->answerkerja3;
		$success['answerkerja4']=$User->first()->answerkerja4;
		$success['answerkerja5']=$User->first()->answerkerja5;
		$success['answerkerja6']=$User->first()->answerkerja6;
		$success['answerkerja7']=$User->first()->answerkerja7;
		$success['answerkerja8']=$User->first()->answerkerja8;
		$success['answerkerja9']=$User->first()->answerkerja9;
		$success['answerkerja10']=$User->first()->answerkerja10;
		$success['answerkerja11']=$User->first()->answerkerja11;

		return response()->json($success,200);
	}
	
	public function updateanswer(Request $request){
		$userid = $request->input('userid');
		$answerprofil1 = $request->input('profil1');
		$answerprofil2 = $request->input('profil2');
		$answerprofil3 = $request->input('profil3');
		$answerprofil4 = $request->input('profil4');
		$answerprofil5 = $request->input('profil5');
		$answerprofil6 = $request->input('profil6');
		$answerprofilhp = $request->input('profilhp');
		$answerprofilnpwp = $request->input('profilnpwp');
		$answerprofilkota = $request->input('profilkota');
		if($answerprofil6==null){$pathprofil6="0";}
		else{
			$pathprofil6 = "foto_diri-".time().".png";
			Storage::disk('public')->put($pathprofil6, base64_decode($answerprofil6));}
		$answerprofil7 = $request->input('profil7');
		if($answerprofil7==null){$pathprofil7="0";}
		else{
			$pathprofil7 = "ktp-".time().".png";
			Storage::disk('public')->put($pathprofil7, base64_decode($answerprofil7));}
		$answerprofil8 = $request->input('profil8');
		if($answerprofil8==null){$pathprofil8="0";}
		else{
			$pathprofil8 = "tanda_tangan-".time().".png";
			Storage::disk('public')->put($pathprofil8, base64_decode($answerprofil8));}
		$answerprofil9 = $request->input('profil9');
		if($answerprofil9==null){$pathprofil9="0";}
		else{
			$pathprofil9 = "npwp-".time().".png";
			Storage::disk('public')->put($pathprofil9, base64_decode($answerprofil9));}
		$answerprofil10 = $request->input('profil10');
		if($answerprofil10==null){$pathprofil10="0";}
		else{
			$pathprofil10 = "buku_tabungan-".time().".png";
			Storage::disk('public')->put($pathprofil10, base64_decode($answerprofil10));}
		$answerprofil11 = $request->input('profil11');
		if($answerprofil11==null){$pathprofil11="0";}
		else{
			$pathprofil11 = "spt_terakhir-".time().".png";
			Storage::disk('public')->put($pathprofil11, base64_decode($answerprofil11));}
		
		$answerpendidikan1 = $request->input('pendidikan1');
		$answerpendidikan2 = $request->input('pendidikan2');
		$answerpendidikan3 = $request->input('pendidikan3');
		$answerpendidikan4 = $request->input('pendidikan4');
		$answerpendidikan5 = $request->input('pendidikan5');
		$answerpendidikan6 = $request->input('pendidikan6');
		$answerpendidikan7 = $request->input('pendidikan7');
		$answerpendidikan8 = $request->input('pendidikan8');
		$answerpendidikan9 = $request->input('pendidikan9');
		$answerpendidikan10 = $request->input('pendidikan10');
		if($answerpendidikan10==null){$pathpendidikan10="0";}
		else{
			$pathpendidikan10 = "ijasah_terakhir-".time().".png";
			Storage::disk('public')->put($pathpendidikan10, base64_decode($answerpendidikan10));}
		$answerpendidikan11 = $request->input('pendidikan11');
		if($answerpendidikan11==null){$pathpendidikan11="0";}
		else{
			$pathpendidikan11 = "transkrip_nilai-".time().".png";
			Storage::disk('public')->put($pathpendidikan11, base64_decode($answerpendidikan11));}
		$answerahli1 = $request->input('ahlistring1');
		$answerahli7 = $request->input('ahli7');
		$answerahli8 = $request->input('ahli8');
		$answerahli9 = $request->input('ahli9');
		$answerahli10 = $request->input('ahli10');
		$answerahli11 = $request->input('ahli11');
		if($answerahli11==null){$pathahli11="0";}
		else{
			$pathahli11 = "sertifikat_keahlian-".time().".png";
			Storage::disk('public')->put($pathahli11, base64_decode($answerahli11));}
		$answerahli12 = $request->input('ahli12');
		if($answerahli11==null){$pathahli12="0";}
		else{
			$pathahli12 = "sertifikat_penghargaan-".time().".png";
			Storage::disk('public')->put($pathahli12, base64_decode($answerahli12));}
		$answerahli13 = $request->input('ahli13');
		if($answerahli13==null){$pathahli13="0";}
		else{
		$pathahli13 = "pernyataan_kesediaan-".time().".png";
		Storage::disk('public')->put($pathahli13, base64_decode($answerahli13));}

		$answergaji1 = $request->input('gaji1');

		
		$User = DB::table('answer_register')->where('user_id',$userid)->update(['user_id'=>$userid,'answerprofil1'=>$answerprofil1,'answerprofil2'=>$answerprofil2,'answerprofil3'=>$answerprofil3,'answerprofil4'=>$answerprofil4,'answerprofil5'=>$answerprofil5,
		'answerprofil6'=>$pathprofil6,'answerprofil7'=>$pathprofil7,'answerprofil8'=>$pathprofil8,'answerprofil9'=>$pathprofil9,'answerprofil10'=>$pathprofil10,'answerprofil11'=>$pathprofil11,
		'answerpendidikan1'=>$answerpendidikan1,'answerpendidikan2'=>$answerpendidikan2,'answerpendidikan3'=>$answerpendidikan3,'answerpendidikan4'=>$answerpendidikan4,'answerpendidikan5'=>$answerpendidikan5,'answerpendidikan6'=>$answerpendidikan6,
		'answerpendidikan7'=>$answerpendidikan7,'answerpendidikan8'=>$answerpendidikan8,'answerpendidikan9'=>$answerpendidikan9,'answerpendidikan10'=>$pathpendidikan10,'answerpendidikan11'=>$pathpendidikan11,
		'answerahli1'=>$answerahli1,'answerahli7'=>$answerahli7,'answerahli8'=>$answerahli8,'answerahli9'=>$answerahli9,'answerahli10'=>$answerahli10,'answerahli11'=>$pathahli11,'answerahli12'=>$pathahli12,'answerahli13'=>$pathahli13,
		'answergaji1'=>$answergaji1,'nohp'=>$answerprofilhp,'npwp'=>$answerprofilnpwp,'kota'=>$answerprofilkota]);
		
		$success['success']='1';
		return response()->json($success, 200);
	}


	public function getItem(){
		$array = [];
				
		$client = new Client();
		$base64 = 'Basic '.base64_encode($this->client_id.':'.$this->secret_key);
		// $client->setDefaultOption('headers', array('Authorization' => $base64));
		$res = $client->request('GET', $this->url_api.'index.php/tblitem/get/', [
			'auth' => [$this->client_id, $this->secret_key]
		]);
		$json = json_decode($res->getBody());
		for($i = 0; $i < count($json); $i++){
			$cData		= $json[$i];
			$curr = [
				"id"=>$cData->ItCode,
				"name"=> $cData->ItName
			];
			array_push($array, $curr);
		}
		if($res->getStatusCode() == 200){
			return response()->json($array, 200);
		} else {
			$data = [
				"status"=> false,
				"message"=> 'somethink wrong'
			];
			return response()->json($data, $res->getStatusCode());
		}

	}

	public function getkota1(){
		$array = [];
				
		$client = new Client();
		$base64 = 'Basic '.base64_encode($this->client_id.':'.$this->secret_key);
		// $client->setDefaultOption('headers', array('Authorization' => $base64));
		$res = $client->request('GET', $this->url_api.'index.php/tblcity/get/', [
			'auth' => [$this->client_id, $this->secret_key]
		]);
		$json = json_decode($res->getBody());
		for($i = 0; $i < count($json); $i++){
			$cData		= $json[$i];
			$curr = [
				"id"=>$cData->CityCode,
				"name"=> $cData->CityName
			];
			array_push($array, $curr);
		}
		if($res->getStatusCode() == 200){
			return response()->json($array, 200);
		} else {
			$data = [
				"status"=> false,
				"message"=> 'somethink wrong'
			];
			return response()->json($data, $res->getStatusCode());
		}
	}
	//public function getspk(Request $request){

//	public function getspk(){
		//$email = $request->input('email');
		
//		$array = [];
//		$client = new Client();
//		$base64 = 'Basic '.base64_encode($this->client_id.':'.$this->secret_key);
//		$res = $client->request('GET', $this->url_api.'index.php/tblvendor/get/',['auth' => [$this->client_id, $this->secret_key]]);
//		$json = json_decode($res->getBody());
		
//		for($i = 0; $i<count($json);$i++){
//			if($json[$i]->VdCode==0059){
//				$dd = ["Email"=>$json[$i]->Email,
//						"VdCode"=>$json[$i]->VdCode];
//				$vdcode = $json[$i]->VdCode;
//				array_push($array, $dd);
//			}
//		}
//		if($res->getStatusCode() == 200){
			//return response()->json($json, 200);
//			$get = $client->request('GET',$this->url_api.'index.php/tblpohdr/get/',['auth' => [$this->client_id, $this->secret_key]]);
//			$jsonhdr = json_decode($get->getBody());
//			for($i = 0; $i<count($jsonhdr);i++){
//				if ($jsonhdr[$i]->VdCode==$vdcode){
//					$df=["Doc No"=>$json[$i]->DocNo,
//					"Status"=>$json[$i]->Status];
//					array_push($array,$df);
//				}
//			}
//			if($get->getStatusCode()==200){
//				return response()->json($array,200);
//			}
//			else{$data =[
//			"Status"=>false,
//			"Message"=>'tblpolhdr Error'];
//			return response()->json($data,$get->getStatusCode());
//			}
//		}
//		else{
//			$data = [
//			"Status"=>false,
//			"Message"=> 'tblvendor Error'
//			];
//			return response()->json($data, $res->getStatusCode());
//		}
//	}
	
	
	public function spk(Request $request){
		$user_id = $request->input('user_id');
		$User=DB::table('spk')->where('user_id',$user_id);
		$success['spk']=$User->get();
		return response()->json($success, 200);
	}
	
	public function viewspk(Request $request){
		$id = $request->input('id');
		$User=DB::table('spk')->where('id',$id);
		$success['id']=$User->first()->id;
		$success['user_id']=$User->first()->user_id;
		$success['name']=$User->first()->name;
		$success['status']=$User->first()->status;
		$success['project']=$User->first()->project;
		$success['projectuser']=$User->first()->projectuser;
		$success['deskripsi']=$User->first()->deskripsi;
		$success['foto']=$User->first()->foto;
		return response()->json($success,200);
	}
	
	public function workplan(Request $request){
		$user_id = $request->input('user_id');
		$User = DB::table('workplan')->where('spk_id',$user_id);
		$success['workplan']=$User->get();
		return response()->json($success, 200);
	}
	
	public function worksheet(Request $request){
		$id = $request->input('id');
		$User = DB::table('workplan')->where('id',$id);
		$success['id']=$User->first()->id;
		$success['spk_id']=$User->first()->spk_id;
		$success['name']=$User->first()->name;
		$success['status']=$User->first()->status;
		$success['nospk']=$User->first()->nospk;
		$success['workstatus']=$User->first()->workstatus;
		$success['foto1']=$User->first()->foto1;
		$success['foto2']=$User->first()->foto2;
		return response()->json($success,200);
	}
	public function editworksheet(Request $request){
		$id = $request->input('id');
		$spinner = $request->input('spinner');
		$foto1 = $request->input('foto1');
		if($foto1==null){$pathworksheet1="0";}
		else{
		$pathworksheet1 = "worksheet_1_-".time().".png";
		Storage::disk('public')->put($pathworksheet1, base64_decode($foto1));}
		$foto2 = $request->input('foto2');
		if($foto2==null){$pathworksheet2="0";}
		else{
		$pathworksheet2 = "worksheet_2_-".time().".png";
		Storage::disk('public')->put($pathworksheet2, base64_decode($foto2));}
		
		$User = DB::table('workplan')->where('id',$id)->update(['workstatus'=>$spinner,'foto1'=>$pathworksheet1,'foto2'=>$pathworksheet2]);
		$success['success']="1";
		return response()->json($success,200);
	}
	public function todayworkplan(Request $request){
		$user_id = $request->input('user_id');
		$User = DB::table('workplan')->where('status','On Going');
		$success['todayworkplan']=$User->get();
		return response()->json($success,200);
	}
	public function pesan(Request $request){
		$user_id = $request->input('user_id');
		$User=DB::table('notification')->where('user_id',$user_id);
		$success['pesan']=$User->get();
		return response()->json($success,200);
	}
	
	public function viewpesan(Request $request){
		$id = $request->input('id');
		$User=DB::table('notification')->where('id',$id);
		$success['sender']=$User->first()->sender;
		$success['date']=$User->first()->date;
		$success['pesan']=$User->first()->pesan;

		return response()->json($success,200);
	}
	
}
