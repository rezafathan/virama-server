<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\User;
use Illuminate\Support\Facades\DB;
use Auth;
use File;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\RequestOptions;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $client_id 	= 'mYq3t6w9z$C&F)J@NcRfTjWnZr4u7x!A';
	protected $secret_key	= 's6v9y$B&E)H@McQfTjWnZr4t7w!z%C*F-JaNdRgUkXp2s5v8y/A?D(G+KbPeShVmYq3t6w9z$C&E)H@McQfTjWnZr4u7x!A%D*G-JaNdRgUkXp2s5v8y/B?E(H+MbPeShVmYq3t6w9z$C&F)J@NcRfTjWnZr4u7x!A%D*G-KaPdSgVkXp2s5v8y/B?E(H+MbQeThWmZq3t6w9z$C&F)J@NcRfUjXn2r5u7x!A%D*G-KaPdSgVkYp3s6v9y/B?E(H+MbQeThWmZq4t7w!z%C&F)J@NcRfUjXn2r5u8x/A?D(G-KaPdSgVkYp3s6v9y$B&E)H@MbQeThWmZq4t7w!z%C*F-JaNdRfUjXn2r5u8x/A?D(G+KbPeShVkYp3s6v9y$B&E)H@McQfTjWnZq4t7w!z%C*F-JaNdRgUkXp2s5u8x/A?D(G+KbPeShVmYq3t6w9y$B&E)H@McQfTjWnZr4u7x!A%C*F-JaNdRgUkXp2s5v8y/B?E(G+KbPeShVmYq';
	protected $url_api		= 'http://localhost/viramaAPI/';

    public function __construct()
    {
        $this->middleware('auth');
        // $user = Auth::user();
        // if(isset($user) && $user->is_admin != 1){
        //     Auth::logout();
        //     return redirect('login');
        // }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = DB::table('users')
                ->join('answer_register', 'users.id', '=', 'answer_register.user_id')
                ->where('users.status', '=', 3)
                ->select('users.*', 'answer_register.answergaji1')
                ->get();
        $data = [
            "users"  => $user
        ];
        return view('User.pendaftar', $data);
    }

    public function terdaftar()
    {
        $user = DB::table('users')
                ->join('answer_register', 'users.id', '=', 'answer_register.user_id')
                ->where('users.status', '=', 2)
                ->select('users.*', 'answer_register.answergaji1')
                ->get();
        $data = [
            "users"  => $user
        ];
        return view('User.terdaftar', $data);
    }

    public function detail($user_id){
        $user = User::where('users.id', '=', $user_id)
                ->join('answer_register', 'users.id', '=', 'answer_register.user_id')
                ->select('users.*', 'answer_register.*')
                ->first();

        $pengalaman = DB::table('answer_pengalaman')
                ->where('user_id', '=', $user_id)
                ->select('answer_pengalaman.*')
                ->get();
        
        $question_answerkerja = DB::table('question_answer')
                ->where('question_answer.jawaban', 'LIKE', '%answerkerja%')
                ->select('question_answer.*')
                ->get();
        $question_answer = DB::table('question_answer')
                ->where('question_answer.jawaban', 'NOT LIKE', '%answerkerja%')
                ->select('question_answer.*')
                ->get();

        $data = [
            "users"         => $user->toArray(),
            "pengalaman"    => $pengalaman,
            "question_ans"  => $question_answer,
            "answer_kerja"  => $question_answerkerja
        ];
        return view('User.detail', $data);;
    }

    function getUpload($filename){
        if(Auth::check()){
            $path = storage_path() . '/app/public/' . $filename;
            if(!File::exists($path)) {
                return response()->json(['message' => 'Image not found.'], 404);
            }
            
            $file = File::get($path);
            $type = File::mimeType($path);
            
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            
            return $response;
        } else {
            return response()->json(['message' => 'Resistric area'], 401);
        }
    }

    function accept(Request $request, $user_id){
        $user   = User::find($user_id);
        $skors  = $request->input('skors');
        if(isset($skors) && $skors != ""){
            $user_register = DB::table('answer_register')
                ->where("user_id", "=", $user_id)
                ->first();

            $client = new Client();
            $res = $client->request('GET', $this->url_api.'index.php/tblvendor/last', [
                'auth' => [$this->client_id, $this->secret_key]
            ]);
            $json = json_decode($res->getBody());

            $temp_num   = preg_replace('/[^0-9]/', '', $json->VdCode);
            $letters    = preg_replace('/[^a-zA-Z]/', '', $json->VdCode);
            $numbers    = $temp_num + 1;
            $count_id   = strlen($numbers) + strlen($letters);
            
            if($count_id != strlen($json->VdCode)){
                $a = strlen($temp_num);
                $b = strlen($numbers);
                $h = $a - $b;
                for($l=0; $l < $h; $l++){
                    $numbers = '0'.$numbers;
                }
            }
            $new_uid    = $letters . $numbers;
            $kota_id    = $this->getCityCode($user_register->kota);
            
            $params_vendor = [
                "VdCode"            =>$new_uid,
                "VdName"            =>$user->name,
                "ActInd"            =>"Y",      //y
                "VendorRegisterInd" =>"Y",      //y
                "UserCode"          =>$user->email,     //username
                "VendorRegisterEmail" =>$user->email,
                "ShortName"         =>$user->name,
                "Address"           =>$user_register->answerprofil4,
                "CityCode"          =>$kota_id,     //tbl city
                "IdentityNo"        =>$user_register->npwp,   //no npwp
                "TIN"               =>null,         //null
                "TaxInd"            =>"N",          //Y
                "Phone"             =>null,
                "Email"             =>$user->email,        //email
                "Mobile"            =>$user_register->nohp,
                "CreditLimit"       =>0, //null
                "Fax"               =>null
                
            ];
            $id_header_harga    = $this->generadeQTCode();
            $params_qtHeader = [
                "DocNo"     => $id_header_harga,
                "SystemNo"  =>null,// null
                "DocDt"     =>Date("Ymd"),// tgl
                "Status"    =>"A",//A
                "CancelInd" =>"N",//N
                "VdCode"    =>$new_uid,//relasi vendor
                "PtCode"    =>"TOP30",//payment term
                "CurCode"   =>"IDR",//IDR - currency
                "Remark"    =>null,//keterangan 400char
            ];
            $str_ahli = str_replace("[","",$user_register->answerahli1);
            $str_ahli = str_replace("]","",$str_ahli);
            $ahli       = explode(",", $str_ahli);
            $params_dtl  = [];

            for($i=0; $i < count($ahli); $i++){
                $urut   = $i+1;
                $itCode = $ahli[$i];
                for($l=0; $l < (3-strlen($urut)); $l++){
                    $urut   = "0".$urut;
                }
                $harga = str_replace(".","",$user_register->answergaji1);
                $harga = str_replace(",","",$harga);
                $params = [
                    "DocNo"     =>$id_header_harga, // header
                    "DNo"       =>$urut, // 
                    "ItCode"    =>$itCode,//relasi item
                    "ActInd"    =>"Y",//Y
                    "UPrice"    =>$harga,//harga
                    "Remark"    =>null// keterangan
                    
                ];
                array_push($params_dtl, $params);
            }
            
            $data = [
                "vendor"    => $params_vendor,
                "qtHDR"     => $params_qtHeader,
                "qtDTL"     => $params_dtl
            ];
            
            try {
                $response = $client->request("POST", $this->url_api."index.php/tblvendor/inputAllVendor" ,[
                    'auth' => [$this->client_id, $this->secret_key],
                    'json' => $data
                ]);

                $data_hasil = json_decode($response->getBody());
                if(isset($data_hasil->status) && $data_hasil->status == 1){
                    $user->skors    = $skors;
                    $user->status   = 2;
                    $user->save();
                }
                
                return redirect('/');
            } catch (RequestException $e){
                return redirect('/detail/'.$user_id);
            }
            
            
        } else {
            return redirect('/detail/'.$user_id);
        }
        
        
    }

    function reject($user_id){
        $user   = User::find($user_id);
        $user->status   = 1;
        $user->save();
        return redirect('/');
    }

    private function generadeQTCode(){
        // == 0001/VIR/QT/08/18
        $client = new Client();
        $res = $client->request('GET', $this->url_api.'index.php/tblqthdr/last/', [
            'auth' => [$this->client_id, $this->secret_key]
        ]);
        $json = json_decode($res->getBody());
        $arr = explode("/", $json->DocNo);
        $last_number= $arr[0];
        $last_bulan = $arr[3];
        $last_th    = $arr[4];

        $current_tgl    = date('m/y');
        $currentBln     = date('m');
        $count_id       = 4;
        if($currentBln > $last_bulan){
            $number_hasil   = 1;
        }
        $number_hasil   = $last_number + 1;

        if($count_id != strlen($number_hasil)){
            $a = 4;
            $b = strlen($number_hasil);
            $h = $a - $b;
            for($l=0; $l < $h; $l++){
                $number_hasil = '0'.$number_hasil;
            }
        }

        return $number_hasil."/VIR/QT/".$current_tgl;
    }

    private function getCityCode($name){
        // == 0001/VIR/QT/08/18
        $client = new Client();
        $res = $client->request('POST', $this->url_api.'index.php/tblcity/search', [
            'auth'      => [$this->client_id, $this->secret_key],
            'form_params'    => ["name"=>$name]
        ]);
        $json = json_decode($res->getBody());
        
        if(isset($json->CityCode)){
            return $json->CityCode;
        } else {
            return null;
        }
        
    }

    function logout(){

    }

	// public function home()
    // {
    //     return view('main');
    // }
}
