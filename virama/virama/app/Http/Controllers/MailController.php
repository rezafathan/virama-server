<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\SendEmail;
use Illuminate\Support\Facades\Mail;
use Response;
use DB;



class MailController extends Controller
{
    public function send(Request $request)
	{
		$email = $request->input('email');
		$pp = $this->quickRandom(8);
		$password = bcrypt($pp);
		$User = DB::table('users')->where('email', $email)->update(['password'=>$password]);
		if(!$User){
			$success['success']='0';
			return response()->json($success,200);
		}
		else{


		$objDemo = new \stdClass();
		$objDemo->demo_one = $pp;
		$objDemo->sender = 'Admin Virama Karya';
		$objDemo->receiver = $email;
		
		Mail::to($email)->send(new SendEmail($objDemo));
		$success['success']='1';
		$success['user']=$email;
		return response()->json($success,200);
		}
	}
	
	public static function quickRandom($length = 16)
	{
		$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
	}

}
