<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

	Route::post('register',['uses' =>'LoginController@store']);
	
	Route::post('login',['uses'=>'LoginController@login']);
	




	






	Route::group(['middleware'=>'auth:api'], function(){
		//Route::get('details',['uses'=>'LoginController@details']);
		//Route::get('kota',['uses'=>'LoginController@getkota']);
		//Route::get('item',['uses'=>'LoginController@getItem']);
		Route::post('upload',['uses'=>'LoginController@upload']);
		Route::post('uploadkerja',['uses'=>'LoginController@uploadkerja']);
		Route::post('forgot',['uses'=>'LoginController@forgot']);
		Route::post('change',['uses'=>'LoginController@change']);
		Route::post('viewprofile',['uses'=>'LoginController@viewprofile']);
		Route::post('edit',['uses'=>'LoginController@edit']);
		Route::post('rejectprofil',['uses'=>'LoginController@rejectprofil']);
		Route::post('rejectpendidikan',['uses'=>'LoginController@rejectpendidikan']);
		Route::post('rejectahli',['uses'=>'LoginController@rejectahli']);
		Route::post('rejectgaji',['uses'=>'LoginController@rejectgaji']);
		Route::post('rejectjasa',['uses'=>'LoginController@rejectjasa']);
		Route::post('rejectkerja',['uses'=>'LoginController@rejectkerja']);
		Route::post('updateanswer',['uses'=>'LoginController@updateanswer']);
		Route::post('updatekerja',['uses'=>'LoginController@updatekerja']);
		Route::post('spk',['uses'=>'LoginController@spk']);
		Route::post('viewspk',['uses'=>'LoginController@viewspk']);
		Route::post('workplan',['uses'=>'LoginController@workplan']);
		Route::post('worksheet',['uses'=>'LoginController@worksheet']);
		Route::post('editworksheet',['uses'=>'LoginController@editworksheet']);
		Route::post('todayworkplan',['uses'=>'LoginController@todayworkplan']);
		Route::post('pesan',['uses'=>'LoginController@pesan']);
		Route::post('viewpesan',['uses'=>'LoginController@viewpesan']);
		Route::post('message',['uses'=>'LoginController@message']);


		Route::get('item',['uses'=>'LoginController@getItem']);
		Route::get('kota',['uses'=>'LoginController@getKota1']);
		Route::post('getspk',['uses'=>'LoginController@getspk']);
		Route::post('getviewspk',['uses'=>'LoginController@getViewSPK']);
		Route::post('getworkplan',['uses'=>'LoginController@getworkplan']);
		Route::post('gettodayworkplan',['uses'=>'LoginController@gettodayworkplan']);
		Route::post('insertpesan',['uses'=>'LoginController@insertpesan']);
		Route::post('send_notification',['uses'=>'LoginController@send_notification']);
		Route::get('storage/{filename}', 'LoginController@getImage')->name('storage');
    

		// Route::get('itemx',['uses'=>'LoginController@getItemx']);
		// Route::get('kotax',['uses'=>'LoginController@getkotax']);
	});


//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
