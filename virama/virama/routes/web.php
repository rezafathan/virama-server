<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::group(['middleware'=>'auth'], function(){
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/terdaftar', 'HomeController@terdaftar')->name('terdaftar');
    Route::get('/detail/{user_id}', 'HomeController@detail')->name('detail');
    Route::post('/accept/{user_id}', 'HomeController@accept')->name('accept');
    Route::get('/reject/{user_id}', 'HomeController@reject')->name('reject');
    Route::get('/media/{filename}', 'HomeController@getUpload')->name('media');
    // Route::get('/main', 'HomeController@home')->name('main');
    Route::post('mail/send','MailController@send');
    
});
Route::get('/privacy', function(){
    return view('privacy');
 });
Route::get('/logout', function(){
    Auth::logout();
    return Redirect::to('login');
 });

