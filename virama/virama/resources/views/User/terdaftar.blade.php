@extends('template')

@section('content')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Data Table</strong>
                </div>
                <div class="card-body">
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
                <tr>
                <th>Name</th>
                <th>email</th>
                <th>gaji</th>
                <th>action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($users as $u)
                <tr>
                <td>{{ $u->name }}</td>
                <td>{{ $u->email }}</td>
                <td>Rp {{ number_format($u->answergaji1,2,',','.') }}</td>
                <td><a href="/detail/{{$u->id}}" class="btn btn-info btn-sm" ><i class="menu-icon fa fa-info"></i> Detail</a>  </td>
                </tr>
            @endforeach
            </tbody>
            </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
