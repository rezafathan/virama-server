@extends('template')

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Profile data diri</strong>
                    </div>
                    <div class="card-body">
                        <div class="form-horizontal">
                            @foreach($question_ans as $q)
                            <div class="row form-group">
                                <div class="col col-md-5"><label class="form-control-label">{{$q->pertanyaan}}</label></div>
                                @if($q->type == 1)
                                <div class="col-12 col-md-12">
                                    <image src="{{ URL::asset('/media/'.$users[$q->jawaban])}}" height="700px"/>
                                </div>
                                @else
                                <div class="col-7 col-md-7">
                                    <p class="form-control-static">{{$users[$q->jawaban]}}</p>
                                </div>
                                @endif
                            </div>
                            
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @foreach($pengalaman as $ke)
            <?Php $kerja = (array) $ke; ?>
            <?Php $split = explode(", ",$kerja['answerkerja1']); ?>
            @for ($i = 0; $i < sizeof($split); $i++)
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Pengalaman Kerja ke {{$i+1}} </strong>
                    </div>
                    <div class="card-body">
                        <div class="form-horizontal">
                            @foreach($answer_kerja as $q)
                            <?Php $splitx =  explode(",", $kerja[$q->jawaban]); ?>
                            <div class="row form-group">
                                <div class="col col-md-5"><label class="form-control-label">{{$q->pertanyaan}}</label></div>
                                @if($q->type == 1)
                                <div class="col-12 col-md-12">
                                    <image src="{{ URL::asset('/media/'.$splitx[$i])}}" height="700px"/>
                                </div>
                                @else
                                <div class="col-7 col-md-7">
                                    <?Php try { ?>
                                    <p class="form-control-static">{{$splitx[$i]}}</p>
                                    <?Php } catch (Exteption $e){ ?>
                                        -
                                    <?Php } ?>
                                </div>
                                @endif
                            </div>
                            
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @endfor
            @endforeach
            @if($users['status'] == 3)
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" class="form-horizontal" action="/accept/{{$users['user_id']}}">
                            @csrf
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label class="form-control-label">
                                        Skors
                                    </label>
                                </div>
                                <div class="col-7 col-md-5">
                                <input id="cc-pament" name="skors" type="text" class="form-control" aria-required="true" aria-invalid="false" value="">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-6">
                                    <button id="payment-button" type="submit" class="btn btn-lg btn-primary btn-block">
                                        <i class="fa fa-check fa-lg"></i>&nbsp;
                                        <span id="payment-button-amount">Accept</span>
                                        <span id="payment-button-sending" style="display:none;">Sending…</span>
                                    </button>
                                </div>
                                <div class="col col-md-6">
                                    <a href="/reject/{{$users['user_id']}}" class="btn btn-lg btn-danger btn-block">
                                        <i class="fa fa-close fa-lg"></i>&nbsp;
                                        <span id="payment-button-amount">Reject</span>
                                        <span id="payment-button-sending" style="display:none;">Sending…</span>
                                    </a>
                                </div>

                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif
            
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
