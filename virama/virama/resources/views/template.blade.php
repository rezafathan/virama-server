<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Smart Surveillance - @yield('title')</title>
    <meta name="description" content="Virama karya, Applikasi Smart Surveillance untuk tenaga kerja">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="{{ URL::asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('css/lib/datatable/dataTables.bootstrap.min.css')}}">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="{{ URL::asset('scss/style.css')}}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body>
        <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src="{{ URL::asset('images/logo.png')}}" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="{{ URL::asset('images/logo2.png')}}" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <h3 class="menu-title">User</h3><!-- /.menu-title -->
                    <li>
                        <a href="{{ route('home') }}"> <i class="menu-icon fa fa-male"></i>Pendaftar </a>
                    </li>
                    <li>
                        <a href=" {{route('terdaftar')}} "> <i class="menu-icon fa fa-users"></i>Terdaftar </a>
                    </li>
                    <li>
                        <a href="{{route('logout')}} "> <i class="menu-icon fa fa-sign-out"></i>Logout </a>
                    </li>
                    
                    
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        @section('header')
            
        @show
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">@yield('page_1', "Home")</a></li>
                            <li><a href="#">@yield('page_2', "Page")</a></li>
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        @yield('content')

    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="{{ URL::asset('js/vendor/jquery-2.1.4.min.js')}}"></script>
    <script src="{{ URL::asset('js/popper.min.js')}} "></script>
    <script src="{{ URL::asset('js/plugins.js')}}"></script>
    <script src="{{ URL::asset('js/main.js')}}"></script>


    <script src="{{ URL::asset('js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{ URL::asset('js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ URL::asset('js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{ URL::asset('js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{ URL::asset('js/lib/data-table/jszip.min.js')}} "></script>
    <script src="{{ URL::asset('js/lib/data-table/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{ URL::asset('js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{ URL::asset('js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{ URL::asset('js/lib/data-table/datatables-init.js')}}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
        } );
    </script>


</body>
</html>
