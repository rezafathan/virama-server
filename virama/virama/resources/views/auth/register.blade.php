@extends('layouts.app')

@section('content')
<body style="background-color: #f0f8ff">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card card-login">
                <!--<div class="card-header">{{ __('Register') }}</div>-->
                
                <div class="card-body ">
                    <div class="logo-login">
                        <div class="title-login">
                            <h3>Register</h3>
                        </div>
                        <img src="{{ URL::asset('images/logo-colored.jpg')}}" alt="Logo">
                    </div>
                    <br>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
						<div class="col-md-8">
                            <label for="name"> <!--class="col-md-4 col-form-label text-md-right">{{ __('Name') }}-->Nama</label>
						</div>
                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
						<div class="col-md-8">
                            <label for="email"><!--{{ __('E-Mail Address') }}-->Email</label>
						</div>
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
						<div class="col-md-8">
                            <label for="password"><!--{{ __('Password') }}-->Password</label>
						</div>
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
						<div class="col-md-8">
                            <label for="password-confirm"><!--{{ __('Confirm Password') }}-->Confirm Password</label>
						</div>
                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0"style="margin-top:30px">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" style="width:100%; height:120%">
                                    <!--{{ __('Register') }}--> REGISTER
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
@endsection
