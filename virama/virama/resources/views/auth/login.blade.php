@extends('layouts.app')

@section('content')
<head>

</head>
<body style="background-color: #f0f8ff">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card card-login">
                

                <div class="card-body">
                    <div class="logo-login">
                        <div class="title-login">
                            <h3>Login Admin</h3>
                        </div>
                        <img src="{{ URL::asset('images/logo-colored.jpg')}}" alt="Logo">
                    </div>
                    <!-- <h3 class="text-center">Login Admin</h3> -->
                    <!-- <p class="text-center">Login to your account</p> -->
                    <br>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
						
                        <div class="form-group row">
							<div class="col-md-8">
                            <label for="email">{{ __('E-Mail Address') }}</label>
							</div>
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"  name="email" required autofocus> <!--value="{{ old('email') }}"-->

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
						<div class="col-md-8">
                            <!--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>-->
							<label for="password">Password</label>
						</div>
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <!--<div class="col-md-6">-->
							<div class="container" style="width:50%">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> <!--{{ __('Remember Me') }}-->Ingat Saya
                                    </label>
                                </div>
                            </div>
							<div class="container" style="width:50%; text-align:right">
								<a class="btn btn-link" href="{{ route('password.request') }}" style="color:#3C95DA">
                                    <!--{{ __('Forgot Your Password?') }}-->Lupa Password?
                                </a>
							</div>
                        </div>

                        <div class="form-group row mb-0" style="margin-top:30px">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" style="width:100%; height:120%">
                                    <!--{{ __('Login') }}-->LOGIN
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
@endsection
